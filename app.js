var express        = require('express')
  , favicon        = require('serve-favicon')
  , swig           = require('swig')
  , bodyParser     = require('body-parser')
  , errorHandler   = require('errorhandler')
  , methodOverride = require('method-override')
  , morgan         = require('morgan')
  , http           = require('http')
  , path           = require('path')
  , db             = require('./models')
  , Entries        = require('./routes/Entries')


var app = express()

// all environments
app.set('port', process.env.PORT || 3000)
app.set('views', __dirname + '/views')

app.engine('swig', swig.renderFile);

app.set('view engine', 'swig');
app.set('views', __dirname + '/views');

app.set('view cache', false);
swig.setDefaults({ cache: false });

app.use(favicon(__dirname + '/public/favicon.ico'))
app.use(morgan('dev'))
app.use(bodyParser())
app.use(methodOverride())
app.use(express.static(path.join(__dirname, 'public')))

// development only
if ('development' === app.get('env')) {
  app.use(errorHandler())
}

app.get('/api/Entries', Entries.findAll)
app.get('/api/Entries/:id', Entries.find)
app.post('/api/Entries', Entries.create)
app.put('/api/Entries/:id', Entries.update)
app.del('/api/Entries/:id', Entries.destroy)

app.get(/^\/(.*)/, function(req, res){
  res.render("layout", {})
});

db
  .sequelize
  .sync()
  .complete(function(err) {
    if (err) {
      throw err
    } else {
      http.createServer(app).listen(app.get('port'), function() {
        console.log('Express server listening on port ' + app.get('port'))
      })
    }
  })
