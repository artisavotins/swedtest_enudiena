angular.module('swedtest_enudiena')
.controller('AgenskalnsPageController', ['$scope', '$location', 'uiGmapGoogleMapApi', function ($scope, $location, uiGmapGoogleMapApi) {
  uiGmapGoogleMapApi.then(function(maps) {
    $scope.map = {
      center: {
        latitude: 56.935295,
        longitude: 24.072102
      },
      zoom: 17,
      markers: [
          {
              location: {
                  latitude: 56.935295,
                  longitude: 24.072102
              },
              id: 'agenskalns',
              name: 'Filiāle "Āgenskalns"',
              address: 'Rīga, Mazā Nometņu iela 27',
              openHours: 'Filiāle - darbdienās 9:00-17:00',
              atmHours: 'Iemaksas automāts (NIB) - darbdienās 9:00-17:00',
              icon: '/img/design/home_icon.png',
              markerClick: function(marker){
                var infoWindow = new google.maps.InfoWindow();
                var contentString  = '<h1>'+marker.model.name+'</h1>'+
                                      '<p class="lead">'+marker.model.address+'</p>'+
                                      '<p class="map-item-popup-info">'+
                                      '<img src="/img/design/home_icon.png" /> '+marker.model.openHours+
                                      '</p>'+
                                      '<p class="map-item-popup-info">'+
                                      '<img src="/img/design/atm_icon.png" /> '+marker.model.atmHours+
                                      '</p>';
                infoWindow.setContent(contentString);
                infoWindow.open($scope.map, marker);
                                      // '<table class="popup">'+
                                      //   '<tbody>'+
                                      //     '<tr>'+
                                      //       '<td class="label">Account:</td>'+
                                      //       '<td class="value">'+ info.officeName +'</td>'+
                                      //     '</tr>'+
                                      //     '<tr>'+
                                      //       '<td class="label">Office Name:</td>'+
                                      //       '<td class="value">'+ info.companyName +'</td>'+
                                      //     '</tr>'+
                                      //     '<tr>'+
                                      //       '<td class="label">Address:</td>'+
                                      //       '<td class="value">'+
                                      //         info.address + ', ' + info.city +
                                      //         '<br />'+ info.countryCode + ' ' + info.postalCode +
                                      //       '</td>'+
                                      //     '</tr>'+
                                      //   '</tbody>'+
                                      // '</table>';
              }
          }
      ]
    };

    //
    // google.maps.event.addListener(marker, 'click', function(){
    //     var contentString  = '<table class="popup">'+
    //         '<tbody>'+
    //           '<tr>'+
    //             '<td class="label">Account:</td>'+
    //             '<td class="value">'+ info.officeName +'</td>'+
    //           '</tr>'+
    //           '<tr>'+
    //             '<td class="label">Office Name:</td>'+
    //             '<td class="value">'+ info.companyName +'</td>'+
    //           '</tr>'+
    //           '<tr>'+
    //             '<td class="label">Address:</td>'+
    //             '<td class="value">'+
    //               info.address + ', ' + info.city +
    //               '<br />'+ info.countryCode + ' ' + info.postalCode +
    //             '</td>'+
    //           '</tr>'+
    //         '</tbody>'+
    //       '</table>';
    //     infoWindow.setContent(contentString);
    //     infoWindow.open($scope.map, marker);
    // });
  });
}]);
