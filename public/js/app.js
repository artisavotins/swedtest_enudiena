// Declare app level module which depends on filters, and services
angular.module('swedtest_enudiena', [
    'ngResource', 'ngRoute', 'ui.bootstrap',
    'ui.bootstrap.showErrors', 'citySelect',
    'uiGmapgoogle-maps'
  ])
  .config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/home/home.html',
        controller: 'HomeController'})
      .when('/pieteikties', {
        templateUrl: 'views/join/form.html',
        controller: 'EntryApplicationController'})
      .when('/paldies', {
          templateUrl: 'views/text/paldies.html',
          controller: 'TextPageController'})
      .when('/filiale-agenskalns', {
          templateUrl: 'views/text/agenskalns.html',
          controller: 'AgenskalnsPageController'})
      .when('/par-swedbank', {
        templateUrl: 'views/text/par-swedbank.html',
        controller: 'TextPageController'})
      .when('/par-enu-dienu', {
        templateUrl: 'views/text/par-enu-dienu.html',
        controller: 'TextPageController'})
      .otherwise({redirectTo: '/'});

    $locationProvider.html5Mode(true);
  }]).filter('range', function(){
    return function(input, total){
      total = parseInt(total);
      for (var i=0; i<total; i++) input.push(i);
      return input;
    }
  }).filter('o_range', function(){
    return function(input, total){
      total = parseInt(total);
      for (var i=1; i<=total; i++) input.push(i);
      return input;
    }
  }).directive('ngConfirmClick', [
    function(){
      return {
        priority: -1,
        restrict: 'A',
        link: function(scope, element, attrs){
          element.bind('click', function(e){
            var message = attrs.ngConfirmClick;
            if(message && !confirm(message)){
              e.stopImmediatePropagation();
              e.preventDefault();
            }
          });
        }
      }
    }
  ]);
