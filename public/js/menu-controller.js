angular.module('swedtest_enudiena')
.controller('MenuController', ['$scope', '$location', function ($scope, $location) {
  $scope.isActive = function (viewLocation) {
    return viewLocation === $location.path();
  };
}]);
