'use strict';

angular.module('swedtest_enudiena')
  .controller('EntryController', ['$scope', '$modal', 'resolvedEntry', 'Entry',
    function ($scope, $modal, resolvedEntry, Entry) {
      
      $scope.Entries = resolvedEntry;

      $scope.create = function () {
        $scope.clear();
        $scope.open();
      };

      $scope.update = function (id) {
        $scope.Entry = Entry.get({id: id});
        $scope.open(id);
      };

      $scope.delete = function (id) {
        Entry.delete({id: id},
          function () {
            $scope.Entries = Entry.query();
          });
      };

      $scope.save = function (id) {
        if (id) {
          Entry.update({id: id}, $scope.Entry,
            function () {
              $scope.Entries = Entry.query();
              $scope.clear();
            });
        } else {
          Entry.save($scope.Entry,
            function () {
              $scope.Entries = Entry.query();
              $scope.clear();
            });
        }
      };

      $scope.clear = function () {
        $scope.Entry = {
          "firstName": "",
          "lastName": "",
          "age": "",
          "city": "",
          "phone": "",
          "email": "",
          "schoolName": "",
          "schoolYear": "",
          "parentFirstName": "",
          "parentLastName": "",
          "parentPhone": "",
          "parentEmail": "",
          "teacherFirstName": "",
          "teacherLastName": "",
          "teacherPhone": "",
          "teacherEmail": "",
          "id": ""
        };
      };

      $scope.open = function (id) {
        var EntrySave = $modal.open({
          templateUrl: 'Entry-save.html',
          controller: 'EntrySaveController',
          resolve: {
            Entry: function () {
              return $scope.Entry;
            }
          }
        });

        EntrySave.result.then(function (entity) {
          $scope.Entry = entity;
          $scope.save(id);
        });
      };
    }])
  .controller('EntrySaveController', ['$scope', '$modalInstance', 'Entry',
    function ($scope, $modalInstance, Entry) {
      $scope.Entry = Entry;

      $scope.ok = function () {
        $modalInstance.close($scope.Entry);
      };

      $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
      };
    }]);
