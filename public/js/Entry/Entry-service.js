'use strict';

angular.module('swedtest_enudiena')
  .factory('Entry', ['$resource', function ($resource) {
    var Entry = $resource('api/Entries/:id', {}, {
      'query': { method: 'GET', isArray: true},
      'get': { method: 'GET'},
      'update': { method: 'PUT'}
    });

    Entry.prototype.getCityDisplay = function(){
      var that = this;

      var cityObject = _.filter(latvianCities, function(el) {
        return el.id == that.city;
      });

      if (cityObject !== undefined){
        return cityObject.pop().title;
      } else {
        return '';
      }
    };

    Entry.prototype.getFormattedCreatedAtDate = function(){
      var dateObject = new Date(this.createdAt);

      var formattedDate = dateObject.getFullYear() + "."
        + _.padLeft((dateObject.getMonth() + 1), 2, '0') + "."
        + _.padLeft(dateObject.getDate(), 2, '0') + "."
        + ' '
        + _.padLeft(dateObject.getHours(), 2, '0') + ':'
        + _.padLeft(dateObject.getMinutes(), 2, '0')

      return formattedDate;
    };

    return Entry;
  }]);
