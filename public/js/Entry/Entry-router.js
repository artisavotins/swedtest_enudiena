'use strict';

angular.module('swedtest_enudiena')
  .config(['$routeProvider', function ($routeProvider) {
    $routeProvider
      .when('/Entries', {
        templateUrl: 'views/Entry/Entries.html',
        controller: 'EntryController',
        resolve:{
          resolvedEntry: ['Entry', function (Entry) {
            return Entry.query();
          }]
        }
      })
    }]);
