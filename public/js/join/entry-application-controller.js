angular.module('swedtest_enudiena')
.controller('EntryApplicationController', ['$scope', '$location', 'Entry'
,function ($scope, $location, Entry) {
  $scope.Entry = {
    firstName: undefined, // req
    lastName: undefined, // req
    age: undefined, // req

    city: undefined, // req
    phone: undefined,
    email: undefined,

    schoolName: undefined, // req
    schoolYear: undefined, // req

    parentFirstName: undefined, // req
    parentLastName: undefined, // req
    parentPhone: undefined, // req
    parentEmail: undefined, // req

    teacherFirstName: undefined, // req
    teacherLastName: undefined, // req
    teacherPhone: undefined, // req
    teacherEmail: undefined, // req

    id: undefined
  };

  // $scope.Entry = {
  //   firstName: "Test", // req
  //   lastName: "Test", // req
  //   age: 17, // req
  //
  //   city: 17, // req
  //   phone: "12345678",
  //   email: "email@test.tld",
  //
  //   schoolName: "Testa Skola", // req
  //   schoolYear: 7, // req
  //
  //   parentFirstName: "Testa", // req
  //   parentLastName: "Mamma", // req
  //   parentPhone: "87654321", // req
  //   parentEmail: "mammas@epasts.tld", // req
  //
  //   teacherFirstName: "Testa", // req
  //   teacherLastName: "Skolotāja", // req
  //   teacherPhone: "62352035", // req
  //   teacherEmail: "skolotaja@skola.edu.tld", // req
  //
  //   id: undefined
  // };

  var formSubmitButton = Ladda.create(document.querySelector('form[name=entryForm] button[type=submit]'));
  $scope.save = function() {
    $scope.$broadcast('show-errors-check-validity');

    if (!!$scope.entryForm.$valid) {
      formSubmitButton.start();

      Entry.save($scope.Entry,
        function (data) {
          $location.path('/paldies');
          $location.replace();
        },
        function (err){
          alert("Kļūda saglabājot! Mēģini vēlreiz!");
          formSubmitButton.stop();
        }
      );
    } else {
      angular.forEach($scope.entryForm.$error.required, function(field) {
        if(!field.$dirty){
          field.$dirty = true;
        }
      });
      alert("Formas lauki, kas apzīmēti ar sarkanu krāsu, satur kļūdas!")
    }
  }
}]);
