angular.module('citySelect', [])
.directive('citySelect', ['$parse', function ($parse) {
  return {
    restrict: 'E',
    template: '<select><option value="">' + _.map(latvianCities, function(c){ return '<option value="'+c.id+'">'+c.title+'</option>' }).join() + '</select>',
    replace: true,
    link: function (scope, elem, attrs) {
      if(!!attrs.emptyLabel){
        $(elem).find('option').first().text(attrs.emptyLabel);
      }

      if (!!attrs.ngModel) {
        var assignCity = $parse(attrs.ngModel).assign;

        elem.bind('change', function (e) {
          assignCity(elem.val());
        });

        // scope.$watch(attrs.ngModel, function (city) {
        //   elem.val(city.id);
        // });
      }
    }
  };
}]);
