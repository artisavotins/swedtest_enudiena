;(function(win) {
  var latvianCities = [
    {"id":0,"title":"Ainaži"}, {"id":1,"title":"Aizkraukle"},
    {"id":2,"title":"Aizpute"}, {"id":3,"title":"Aknīste"},
    {"id":4,"title":"Aloja"}, {"id":5,"title":"Alūksne"},
    {"id":6,"title":"Ape"}, {"id":7,"title":"Auce"},
    {"id":8,"title":"Baldone"}, {"id":9,"title":"Baloži"},
    {"id":10,"title":"Balvi"}, {"id":11,"title":"Bauska"},
    {"id":12,"title":"Brocēni"}, {"id":13,"title":"Cēsis"},
    {"id":14,"title":"Cesvaine"}, {"id":15,"title":"Dagda"},
    {"id":16,"title":"Daugavpils"}, {"id":17,"title":"Dobele"},
    {"id":18,"title":"Durbe"}, {"id":19,"title":"Grobiņa"},
    {"id":20,"title":"Gulbene"}, {"id":21,"title":"Ikšķile"},
    {"id":22,"title":"Ilūkste"}, {"id":23,"title":"Jaunjelgava"},
    {"id":24,"title":"Jēkabpils"}, {"id":25,"title":"Jelgava"},
    {"id":26,"title":"Jūrmala"}, {"id":27,"title":"Kandava"},
    {"id":28,"title":"Kārsava"}, {"id":29,"title":"Krāslava"},
    {"id":30,"title":"Kuldīga"}, {"id":31,"title":"Ķegums"},
    {"id":32,"title":"Lielvārde"}, {"id":33,"title":"Liepāja"},
    {"id":34,"title":"Līgatne"}, {"id":35,"title":"Limbaži"},
    {"id":36,"title":"Līvāni"}, {"id":37,"title":"Lubāna"},
    {"id":38,"title":"Ludza"}, {"id":39,"title":"Madona"},
    {"id":40,"title":"Mazsalaca"}, {"id":41,"title":"Ogre"},
    {"id":42,"title":"Olaine"}, {"id":43,"title":"Pāvilosta"},
    {"id":44,"title":"Piltene"}, {"id":45,"title":"Pļaviņas"},
    {"id":46,"title":"Preiļi"}, {"id":47,"title":"Priekule"},
    {"id":48,"title":"Rēzekne"}, {"id":49,"title":"Rīga"},
    {"id":50,"title":"Rūjiena"}, {"id":51,"title":"Sabile"},
    {"id":52,"title":"Salacgrīva"}, {"id":53,"title":"Salaspils"},
    {"id":54,"title":"Saldus"}, {"id":55,"title":"Saulkrasti"},
    {"id":56,"title":"Seda"}, {"id":57,"title":"Sigulda"},
    {"id":58,"title":"Skrunda"}, {"id":59,"title":"Smiltene"},
    {"id":60,"title":"Staicele"}, {"id":61,"title":"Stende"},
    {"id":62,"title":"Strenči"}, {"id":63,"title":"Subate"},
    {"id":64,"title":"Talsi"}, {"id":65,"title":"Tukums"},
    {"id":66,"title":"Valdemārpils"}, {"id":67,"title":"Valka"},
    {"id":68,"title":"Valmiera"}, {"id":69,"title":"Vangaži"},
    {"id":70,"title":"Varakļāni"}, {"id":71,"title":"Ventspils"},
    {"id":72,"title":"Viesīte"}, {"id":73,"title":"Viļaka"},
    {"id":74,"title":"Viļāni"}, {"id":75,"title":"Zilupe"}
  ];

  /** Used to determine if values are of the language type Object */
  var objectTypes = {
    'boolean': false,
    'function': true,
    'object': true,
    'number': false,
    'string': false,
    'undefined': false
  };

  /** Used as a reference to the global object */
  var root = (objectTypes[typeof window] && window) || this;

  /** Detect free variable `exports` */
  var freeExports = objectTypes[typeof exports] && exports && !exports.nodeType && exports;

  /** Detect free variable `module` */
  var freeModule = objectTypes[typeof module] && module && !module.nodeType && module;

  /** Detect the popular CommonJS extension `module.exports` */
  var moduleExports = freeModule && freeModule.exports === freeExports && freeExports;

  /** Detect free variable `global` from Node.js or Browserified code and use it as `root` */
  var freeGlobal = objectTypes[typeof global] && global;
  if (freeGlobal && (freeGlobal.global === freeGlobal || freeGlobal.window === freeGlobal)) {
    root = freeGlobal;
  }

  if (typeof define == 'function' && typeof define.amd == 'object' && define.amd) {
    // Expose Lo-Dash to the global object even when an AMD loader is present in
    // case module is loaded with a RequireJS shim config.
    // See http://requirejs.org/docs/api.html#config-shim
    root.latvianCities = latvianCities;

    // define as an anonymous module so, through path mapping, it can be
    // referenced as the "underscore" module
    define(function() {
      return latvianCities;
    });
  }
  // check for `exports` after `define` in case a build optimizer adds an `exports` object
  else if (freeExports && freeModule) {
    // in Node.js or RingoJS
    if (moduleExports) {
      (freeModule.exports = latvianCities).latvianCities = latvianCities;
    }
    // in Narwhal or Rhino -require
    else {
      freeExports.latvianCities = latvianCities;
    }
  }
  else {
    // in a browser or Rhino
    root.latvianCities = latvianCities;
  }
}).call(this);
