module.exports = function(sequelize, DataTypes) {
  var Entry = sequelize.define('Entry', {

    firstName: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        len: [3, 20],
      },
    },

    lastName: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        len: [3, 20],
      },
    },

    age: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        min: 1,
        max: 99,
      },
    },

    city: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        min: 1,
      },
    },

    phone: {
      type: DataTypes.STRING,
      allowNull: true,
      validate: {
        len: [8, 8],
      },
    },

    email: {
      type: DataTypes.STRING,
      allowNull: true,
      validate: {
        len: [5, 50],
      },
    },

    schoolName: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        len: [3, 75],
      },
    },

    schoolYear: {
      type: DataTypes.INTEGER,
      allowNull: false,
      validate: {
        min: 1,
        max: 12,
      },
    },

    parentFirstName: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        len: [3, 20],
      },
    },

    parentLastName: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        len: [3, 20],
      },
    },

    parentPhone: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        len: [8, 8],
      },
    },

    parentEmail: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        len: [5, 50],
      },
    },

    teacherFirstName: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        len: [3, 20],
      },
    },

    teacherLastName: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        len: [3, 20],
      },
    },

    teacherPhone: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        len: [8, 8],
      },
    },

    teacherEmail: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        len: [5, 50],
      },
    },
  })

  return Entry
}
